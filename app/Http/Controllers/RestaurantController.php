<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use Session;

class RestaurantController extends Controller
{   
    public function __construct() {

        $this->middleware('auth')->except('create');
       

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //pobieram wszystkie restauracje z bazy
        //$restaurants= Restaurant::all();
        $restaurants= Restaurant::paginate(5);
        return view('restaurants.index')->with('restaurants',$restaurants);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('restaurants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //walidacja
        $this->validate($request, array(
               'nazwa'=>'required',
               'kod'=>'required | regex:/^[0-9]{2}-[0-9]{3}$/',
               'miejscowosc'=>'required',
               'ulica'=>'required',
               'nr'=>'required',
               'email'=>'required |email ',
               'email' => 'unique:restaurants,email',
               'tel'=>'required',
               'gps1'=>['required', 'regex:/^-?((?:1[0-7]|[1-9])?\d(?:\.\d{1,})?|180(?:\.0{1,})?)$/' ],
               'gps2'=>['required', 'regex:/^-?((?:1[0-7]|[1-9])?\d(?:\.\d{1,})?|180(?:\.0{1,})?)$/' ]
              

        ));    
                $restaurant = new Restaurant(); 
                $restaurant->nazwa = $request->nazwa;
                $restaurant->kod = $request->kod;
                $restaurant->miejscowosc = $request->miejscowosc;
                $restaurant->ulica = $request->ulica;
                $restaurant->nr = $request->nr;
                $restaurant->email = $request->email;
                $restaurant->tel = $request->tel;
                $restaurant->gps1= $request->gps1;
                $restaurant->gps2 = $request->gps2;
                $restaurant->www = $request->www;
                $restaurant->save();
               

                Session::flash('success','Restauracja została poprawnie zapisana');
                return redirect()-> route('restaurants.show',$restaurant->id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   $restaurant= Restaurant::find($id);
        return view('restaurants.show')->with('restaurant',$restaurant);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restaurant= Restaurant::find($id);
        return view('restaurants.edit')->with('restaurant',$restaurant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        $restaurant= Restaurant::find($id);
        //walidacja
        $this->validate($request, array(
            'nazwa'=>'required',
            'kod'=>'required | regex:/^[0-9]{2}-[0-9]{3}$/',
            'miejscowosc'=>'required',
            'ulica'=>'required',
            'nr'=>'required',
            'email'=>'required |email ',
            'email' => 'unique:restaurants,email,'.$restaurant->id,     //pomija sprawdzanie unikalnego emaila dla aktualnego id 
            'tel'=>'required',
            'gps1'=>['required', 'regex:/^-?((?:1[0-7]|[1-9])?\d(?:\.\d{1,})?|180(?:\.0{1,})?)$/' ],
            'gps2'=>['required', 'regex:/^-?((?:1[0-7]|[1-9])?\d(?:\.\d{1,})?|180(?:\.0{1,})?)$/' ]
           

        ));  
        
        $restaurant->nazwa = $request->input('nazwa');
        $restaurant->kod = $request->input('kod');
        $restaurant->miejscowosc = $request->input('miejscowosc');
        $restaurant->ulica = $request->input('ulica');
        $restaurant->nr = $request->input('nr');
        $restaurant->email = $request->input('email');
        $restaurant->tel = $request->input('tel');
        $restaurant->gps1= $request->input('gps1');
        $restaurant->gps2 = $request->input('gps2');
        $restaurant->www = $request->input('www');
        $restaurant->save();
        

        Session::flash('success','Restauracja została edytowana');
        return redirect() -> route('restaurants.show',$restaurant->id);




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restaurant= Restaurant::find($id);
        $restaurant->delete();
        Session::flash('success','Restauracja została usunięta');
        return redirect()->route('restaurants.index');

    }
}
