<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Post;

class BlogController extends Controller
{
    public function index()
    {   
        $posts = Post::orderBy('id', 'desc')->paginate(3);
       // $posts= DB::table('posts')->orderBy('id', 'desc')->get();
        return view('blog.index')->with('posts',$posts);
       
    }

    public function single($id)
    {   
        $post=Post::find($id);
        return view('blog.single')->with('post',$post);
       
    }





    
    public function about()
    {
        return view('pages.about');

    }
    

}
