<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Session;
use Image;

class PostController extends Controller
{   
    public function __construct() {

        $this->middleware('auth');


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts= Post::paginate(5);
        return view('posts.index')->with('posts',$posts);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // wyświetli formularz do wpisu 
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $this->validate($request, array(
            'title'=>'required | max:255',
            'body'=>'required',
            
           

     ));    
             $post = new Post(); 
             $post->title = $request->title;
             $post->body = $request->body;
             if($request->hasFile('image')){
                $image= $request->file('image');
                $filename= time(). '.'. $image->getClientOriginalExtension(); 
                $filename2= 'min'.$filename;
                $location= public_path('images/'. $filename);
                $location2= public_path('images/'. $filename2);
                Image::make($image)->resize(800, 400)->save($location);
                Image::make($image)->resize(400, 200)->save($location2);
                $post->image=$filename;   
             }
            
             $post->save();
            

             Session::flash('success','Post został poprawnie zapisany');
             return redirect()-> route('posts.show',$post->id);

    




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post= Post::find($id);
        return view('posts.show')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post= Post::find($id);
        return view('posts.edit')->with('post',$post);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $this->validate($request, array(
            'title'=>'required | max:255',
            'body'=>'required',
            

     ));  
     $post= Post::find($id);
     $post->title = $request->input('title');
     $post->body = $request->input('body');
    
     $post->save();
     

     Session::flash('success','Post został edytowany');
     return redirect() -> route('posts.show',$post->id);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $post= Post::find($id);
        $post->delete();
        Session::flash('success','Post został usunięty');
        return redirect()->route('posts.index');
    }
}
