@if (Session::has('success')) 

<div class="alert alert-success" role="alert">
 <strong>Sukces:</strong> {{Session::get('success')}}
</div>

@endif


@if ($errors->any())

    <div class="alert alert-danger" role="alert">
    <strong>Błędy:</strong>
    <ul> 
    @foreach ($errors->all() as $error)
           <li>{{$error}}</li>
    @endforeach
    </ul>


</div>  

@endif
