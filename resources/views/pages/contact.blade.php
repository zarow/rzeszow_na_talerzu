@extends('master')

@section('content')

  <!-- Table -->

    <div class="row">

        <table class="table table-striped">
            <thead>
                <tr >
                    <th scope="col">#</th>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Ulica</th>
                    <th scope="col">Nr</th>
                    <th scope="col">Kod</th>
                    <th scope="col">Miejscowość</th>
                    <th scope="col">Telefon</th>
                    <th scope="col">GPS1</th>
                    <th scope="col">GPS2</th>
                    <th scope="col">Strona www</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Akcje</th>
                </tr>
            </thead>
            <tbody>
            
            </tbody>
        </table>
   
    </div>
@stop
