<!DOCTYPE html>
<html lang="pl">
<head>
  <title>Rzeszów na talerzu</title>
  <meta charset="utf-8">
  <meta name="description" content="CMS" />
  <meta name="author" content="Grzegorz Żarów" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="shortcut icon" type="img/png" href="img/restaurant.png"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href={{URL::asset('css/admin.css')}}>
  <link rel="stylesheet" href={{URL::asset('css/parsley.css')}}>
  <link rel="stylesheet" href={{URL::asset('css/clean-blog.css')}}> 
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>

<body>
    
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="{{url('/')}}"><i class="fas fa-utensils" aria-hidden="true"></i> Rzeszów na talerzu</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="{{url('/')}}">Strona główna</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('/posts')}}">Admin</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('/contact')}}">Kontakt</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('/about')}}">O mnie</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('../img/banner_1.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>@yield('title') </h1>
              <span class="subheading">@yield('subtitle') </span>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 mx-auto">
             
          @yield('content')
           
        </div>
      </div>
    </div>

    <hr>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <ul class="list-inline text-center">
              <li class="list-inline-item">
                <a href="#">
                   <i class="fab fa-facebook" style="font-size:50px"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                   <i class="fab fa-twitter" style="font-size:50px"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                    <i class="fab fa-github" style="font-size:50px"></i>
                </a>
              </li>
            </ul>
            <p class="copyright text-muted">Copyright &copy; Grzegorz Żarów 2018</p>
          </div>
        </div>
      </div>
    </footer>


<script src={{URL::asset('js/jquery.js')}}></script>
<script src={{URL::asset('js/bootstrap.bundle.min.js')}}></script>
<script src={{URL::asset('js/clean-blog.min.js')}}></script>
</body>


</html>