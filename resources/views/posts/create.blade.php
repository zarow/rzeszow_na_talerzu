@extends('master')
@section('title',' Utwórz nowy post')
@section('content')



 {!! Form::open(['route' => 'posts.store','data-parsley-validate'=>'', 'files'=>true]) !!}

    <div class="row">
      <div class="col-md-12">
          <div class="form-group">
           {{Form::label('title','Tytuł:')}}
           {{Form::text('title',null,array('class'=>'form-control','placeholder'=>'Tytuł postu','required'=>'')) }}               
          </div>
          <div class="form-group">
            {{Form::label('body','Treść:')}}
            {{Form::textarea('body',null,array('class'=>'form-control','placeholder'=>'Treść postu','required'=>'')) }}
          </div>
           <div class="form-group">
            {{Form::label('image','Wczytaj zdjęcie:')}}
            <br>
            {{Form::file('image')}}
           </div>
      </div>        
      <br/>
        
      <div class="col-12 text-center">
          {{Form::submit('Zatwierdź',array('class'=>'btn btn-success btn-block'))}}
          
      </div>
    </div>  
 
    
{!! Form::close() !!}

@endsection