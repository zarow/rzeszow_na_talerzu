@extends('master')
@section('title'," Edytuj post nr $post->id ")
@section('content')



 {!! Form::model($post,['route' => ['posts.update',$post->id],'data-parsley-validate'=>'','method'=>'PUT']) !!}

    <div class="row">
      <div class="col-md-12">
          <div class="form-group">
           {{Form::label('title','Tytuł')}}
           {{Form::text('title',null,array('class'=>'form-control','placeholder'=>'Tytuł postu','required'=>'')) }}               
          </div>
          <div class="form-group">
            {{Form::label('body','Treść')}}
            {{Form::textarea('body',null,array('class'=>'form-control','placeholder'=>'Treść postu','required'=>'')) }}
          </div>
      </div>        
      <br/>
        
      <div class="col-12 text-center">
          {{Form::submit('Zapisz zmiany',array('class'=>'btn btn-success btn-block'))}}
          
      </div>
    </div>  
 
    
{!! Form::close() !!}

@endsection