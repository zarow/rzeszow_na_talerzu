@extends('master')
@section('title',' Wszystkie posty')
@section('content')


  <!-- Table -->
    <div class="container">
        <div class="row">

            <table class="table table-striped">
                <thead>
                    <tr >
                        <th scope="col" class="text-center">#</th>
                        <th scope="col" class="text-center">Temat</th>
                        <th scope="col" class="text-center">Treść</th>
                        <th scope="col" class="text-center">Obrazek</th>
                        <th scope="col" class="text-center">Akcje</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr> 
                            <th>{{$post->id}}</th>
                            <td>{{$post->title}} </td>
                            <td>{{$post->body}} </td>
                            <td>{{$post->image}} </td>
                        
                            
                            <td>
                                <div class="d-flex">
                                    {!!Html::linkRoute('posts.show', 'Pokaż',array($post->id),array('class'=>'btn btn-outline-success btn-sm ml-1'))!!}
                                    {!!Html::linkRoute('posts.edit', 'Edytuj',array($post->id),array('class'=>'btn btn-outline-primary btn-sm ml-1'))!!} 
                                    {!! Form::open(['route' => ['posts.destroy',$post->id],'method'=>'DELETE','style'=>'display:inline-block']) !!}
                                    {{Form::submit('Usuń',array('class'=>'btn btn-outline-danger btn-sm ml-1'))}}
                                    {!! Form::close() !!} 
                                </div>
                            </td>
                        </tr>
                        
                    @endforeach    

                
                </tbody>
            </table>
            <div class="mx-auto">
                {!!$posts->links();!!}
            </div>
        </div>
   </div>






@endsection