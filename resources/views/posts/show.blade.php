@extends('master') 
@section('title'," Widok postu nr $post->id") 
@section('content')

<p><strong>Tytuł: </strong>{{$post->title}}</p>
<p><strong>Treść: </strong>{{$post->body}} </p>
<div class="row">
{!!Html::linkRoute('posts.edit', 'Edytuj',array($post->id),array('class'=>'btn btn-outline-primary m-1'))!!}
{!! Form::open(['route' => ['posts.destroy',$post->id],'method'=>'DELETE']) !!}
{{Form::submit('Usuń',array('class'=>'btn btn-outline-danger m-1'))}}
{!! Form::close() !!}
</div>
@endsection