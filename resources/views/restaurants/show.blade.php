@extends('master')
@section('title',' Widok restauracji')
@section('content')

<p><strong>Nazwa: </strong>{{$restaurant->nazwa}}</p>
<p><strong>Ulica: </strong>{{$restaurant->ulica}} {{$restaurant->nr}}</p>
<p><strong>Kod: </strong>{{$restaurant->kod}} {{$restaurant->miejscowosc}}</p>
<p><strong>Telefon: </strong>{{$restaurant->tel}}</p>
<p><strong>Email: </strong>{{$restaurant->email}}</p>
<p><strong>Wpółrzędne GPS1: </strong>{{$restaurant->gps1}}</p>
<p><strong>Wpółrzędne GPS2: </strong>{{$restaurant->gps2}}</p>
<p><strong>Stron www: </strong>{{$restaurant->www}}</p>
<p><strong>Utworzone: </strong><i>{{$restaurant->created_at}}</i></p>
<p><strong>Ostatnio edytowane: </strong><i>{{$restaurant->updated_at}}</i></p>
<div class="row">
{!!Html::linkRoute('restaurants.index', 'Wszystkie',array(),array('class'=>'btn btn-outline-success m-1'))!!}
{!!Html::linkRoute('restaurants.edit', 'Edytuj',array($restaurant->id),array('class'=>'btn btn-outline-primary m-1'))!!}
{!! Form::open(['route' => ['restaurants.destroy',$restaurant->id],'method'=>'DELETE']) !!}
{{Form::submit('Usuń',array('class'=>'btn btn-outline-danger m-1'))}}
{!! Form::close() !!}
</div>
@endsection