@extends('master') @section('title',' Wszystkie restauracje') 
@section('content')
<!-- Table -->
<div class="container">
    <div class="row">

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col" class="text-center">#</th>
                    <th scope="col" class="text-center">Nazwa</th>
                    <th scope="col" class="text-center">Ulica</th>
                    <th scope="col" class="text-center">Nr</th>
                    <th scope="col" class="text-center">Kod</th>
                    <th scope="col" class="text-center">Miejscowość</th>
                    <th scope="col" class="text-center">Telefon</th>
                    <th scope="col" class="text-center">E-mail</th>
                    <th scope="col" class="text-center">Strona www</th>
                    <th scope="col" class="text-center">Akcje</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($restaurants as $restaurant )
                <tr>
                    <th>{{$restaurant->id}}</th>
                    <td>{{$restaurant->nazwa}} </td>
                    <td>{{$restaurant->ulica}} </td>
                    <td>{{$restaurant->nr}} </td>
                    <td>{{$restaurant->kod}} </td>
                    <td>{{$restaurant->miejscowosc}} </td>
                    <td>{{$restaurant->tel}} </td>
                    <td>{{$restaurant->email}} </td>
                    <td>{{$restaurant->www}} </td>

                    <td>
                        <div class="d-flex">
                            {!!Html::linkRoute('restaurants.show', 'Pokaż',array($restaurant->id),array('class'=>'btn btn-outline-success btn-sm ml-1'))!!}
                            {!!Html::linkRoute('restaurants.edit', 'Edytuj',array($restaurant->id),array('class'=>'btn btn-outline-primary
                            btn-sm ml-1'))!!} {!! Form::open(['route' => ['restaurants.destroy',$restaurant->id],'method'=>'DELETE','style'=>'display:inline-block'])
                            !!} {{Form::submit('Usuń',array('class'=>'btn btn-outline-danger btn-sm ml-1'))}} {!! Form::close()
                            !!}
                        </div>
                    </td>
                </tr>

                @endforeach

            </tbody>
        </table>
        <div class="mx-auto">
            {!!$restaurants->links();!!}
        </div>
    </div>
</div>
@endsection