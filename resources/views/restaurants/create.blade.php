@extends('master')
@section('title',' Utwórz nową restaurację')

@section('content')
<br><br>
 {!! Form::open(['route' => 'restaurants.store','data-parsley-validate'=>'']) !!}

      <div class="row">
        <div class="col-md-6">
         <div class="form-group">
           {{Form::label('nazwa','Nazwa restauracji')}}
           {{Form::text('nazwa',null,array('class'=>'form-control','placeholder'=>'Nazwa Twojej restauracji','required'=>'')) }}
          </div>
          <div class="form-group">
           {{Form::label('kod','Kod pocztowy')}}
              
              
           {{Form::text('kod',null,array('class'=>'form-control','placeholder'=>'Kod pocztowy','required'=>'','pattern'=>'/^[0-9]{2}-[0-9]{3}$/')) }}
          </div>
          <div class="form-group">
           {{Form::label('miejscowosc','Miejscowość')}}
           {{Form::text('miejscowosc',null,array('class'=>'form-control','placeholder'=>'Miejscowość','required'=>'')) }}
          </div>
            <div class="form-group">
            {{Form::label('ulica','Ulica')}}
            {{Form::text('ulica',null,array('class'=>'form-control','placeholder'=>'Ulica','required'=>'')) }}

            </div>

          <div class="form-group">
            {{Form::label('nr','Numer')}}
            {{Form::text('nr',null,array('class'=>'form-control','placeholder'=>'Numer','required'=>'')) }}

          </div>


        </div>
        <div class="col-md-6">
           <div class="form-group">
            {{Form::label('email','Email')}}
            {{Form::email('email',null,array('class'=>'form-control','placeholder'=>'Wpisz email','required'=>'','type'=>'email')) }}
          </div>

          <div class="form-group">
            {{Form::label('tel','Telefon')}}
            {{Form::text('tel',null,array('class'=>'form-control','placeholder'=>'Telefon','required'=>'')) }}
          </div>
          <div class="form-group">
            {{Form::label('gps1','GPS1')}}
            {{Form::text('gps1',null,array('class'=>'form-control','placeholder'=>'GPS1 (Google Maps)','required'=>'','pattern'=>'/^-?((?:1[0-7]|[1-9])?\d(?:\.\d{1,})?|180(?:\.0{1,})?)$/')) }}
          </div>
          <div class="form-group">
            {{Form::label('gps2','GPS2')}}
            {{Form::text('gps2',null,array('class'=>'form-control','placeholder'=>'GPS2 (Google Maps)','required'=>'','pattern'=>'/^-?((?:1[0-7]|[1-9])?\d(?:\.\d{1,})?|180(?:\.0{1,})?)$/')) }}         
         </div>
          <div class="form-group">
            {{Form::label('www','Strona www')}}
            {{Form::text('www',null,array('class'=>'form-control','placeholder'=>'Podaj adres strony www')) }}  
          </div>

        </div>
        
      </div>
      <br/>
      <div class="row ">
        <div class="col-12 text-center">
          {{Form::submit('Zarejestruj się',array('class'=>'btn btn-primary'))}}
        </div>



      </div>

 
    
{!! Form::close() !!}










@endsection