<!DOCTYPE html>
<html lang="pl">
<head>
  <title>Rzeszów na talerzu</title>
  <meta charset="utf-8">
  <meta name="description" content="CMS" />
  <meta name="author" content="Grzegorz Żarów" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href={{URL::asset('css/admin.css')}}>
  <link rel="stylesheet" href={{URL::asset('css/parsley.css')}}>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>

<body >
    <div class="container">
            <!-- Header -->
            <div class="row">
                <div class="col-md-12 text-left">
                    <h3><i class="fas fa-utensils" aria-hidden="true"></i> Restauracje - Panel Administracyjny @yield('title')</h3><br/>
                </div>
            </div>
            <!-- End of Header -->
        @include('partials.messages')

        {{ Auth::check() ? "Witaj ". Auth::user()->name . " !"   : "Wylogowany" }}

        @yield('content')


        <div class="row">
            <div class="col xs-12 text-center">
                <footer class="small">
                    <hr>
                    <small>&copy; Grzegorz Żarów</small>
                </footer>
            </div>
        </div>
        

    </div>
<!-- Scripts -->
<script src={{URL::asset('js/jquery.js')}}></script>
<script src={{URL::asset('js/parsley.min.js')}}></script>


</body>


</html>
