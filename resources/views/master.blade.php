<!DOCTYPE html>
<html lang="pl">
<head>
  <title>Rzeszów na talerzu</title>
  <meta charset="utf-8">
  <meta name="description" content="CMS" />
  <meta name="author" content="Grzegorz Żarów" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="shortcut icon" type="img/png" href="img/restaurant.png"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href={{URL::asset('css/admin.css')}}>
  <link rel="stylesheet" href={{URL::asset('css/parsley.css')}}>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>

<body >
    <div class="container">
            <!-- Header -->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><i class="fas fa-utensils" aria-hidden="true"></i>    Panel Administracyjny -@yield('title') </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active navbar-right">
        <a class="nav-link btn btn-success btn-sm" href="{{url('/posts/create')}}">Dodaj post <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link btn btn-info btn-sm ml-2" href="{{url('/restaurants/create')}}">Dodaj restaurację</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Wybierz
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{url('/restaurants')}}">Restauracje</a>
          <a class="dropdown-item" href="{{url('/posts')}}">Posty</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{url('/logout')}}">Wyloguj</a>
        </div>
      </li>
     
    </ul>
   
  </div>
</nav>
            <!-- End of Header -->
        @include('partials.messages')
        <br>
        <span style="color: blue">
        {{ Auth::check() ? "Witaj ". Auth::user()->name . " !"   : "Wylogowany" }}
        </span>
        @yield('content')


        <div class="row">
            <div class="col xs-12 text-center">
                <footer class="small">
                    <hr>
                    <small>&copy; Grzegorz Żarów</small>
                </footer>
            </div>
        </div>
        

    </div>
<!-- Scripts -->
<script src={{URL::asset('js/jquery.js')}}></script>
<script src={{URL::asset('js/parsley.min.js')}}></script>
<script src={{URL::asset('js/bootstrap.bundle.min.js')}}></script>


</body>


</html>
