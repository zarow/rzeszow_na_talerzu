@extends('master-blog')
@section('title', 'Kulinarny Blog')
@section('subtitle', "$post->title")
@section('content')

  <article>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 mx-auto">
          <h1>{{$post->title}}</h1>
          @if($post->image)
          <img src="{{asset('images/'.$post->image)}}">
           @endif
           <br><br>
           <h5 style="font-weight: normal">{{$post->body}}<h5>
           
          </div>
        </div>
      </div>
      <p class="post-meta text-right"style="font-size:16px"><i>Wysłane przez Grzegorz Żarów {{$post->created_at}}</i></p>
    </article>





@endsection