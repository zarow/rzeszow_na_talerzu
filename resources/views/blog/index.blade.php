@extends('master-blog')
@section('title', 'Kulinarny Blog')
@section('subtitle', 'Subiektywnie o jedzeniu...')

@section('content')

 @foreach ($posts as $post )
    <div class="row">
        <div class="col-5 mini">
         @if($post->image)
          <img src="{{asset('images/'.'min'.$post->image)}}">
           @endif
        </div>
        <div class="col-7">
            <div class="post-preview">
                <a href="/blog/{{$post->id}}">
                <h2 class="post-title">
                {{$post->title}}
                </h2>
                <h3 class="post-subtitle">
                {{substr($post->body,0,180)}} {{strlen($post->body)>180 ? "..." : "" }}
                
                </h3>
                </a>
                <p class="post-meta text-right">Wysłane przez Grzegorz Żarów {{$post->created_at}}</p>
            </div>
        </div> 
    </div>        
<hr>
@endforeach
 <div class="row">   
    <div class="mx-auto">
                {!!$posts->links();!!}
    </div>
 </div>   
@endsection


